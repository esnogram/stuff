import requests
from lxml import html
import sys
import urllib
from urllib.parse import urljoin

response = requests.get('https://topbrands.ru/zhenskie-kollektsii/odezhda/')
parsed_body = html.fromstring(response.text)

images = parsed_body.xpath('//img/@src')
if not images:
	sys.exit("Found No Images")

images = [urljoin(response.url, url) for url in images]
print ('Found %s images' % len(images))

a=1
for url in images:
	r = requests.get(url)
	#f = open('downloaded_images/'+str(a)+'%s' % url.split('/')[-1], 'wb')
	f = open('downloaded_images/%s' % url.split('/')[-1], 'wb')
	a+=1
	f.write(r.content)
	f.close()