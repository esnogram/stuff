import urllib.request
from bs4 import BeautifulSoup
import requests

BASE_URL = 'https://www.lamoda.ru/c/477/clothes-muzhskaya-odezhda/?siteli..'


def get_html(url):
	response = urllib.request.urlopen(url)
	return response.read()


def parse(html):
	soup = BeautifulSoup(html, "html.parser")
	table = soup.find_all('img', class_='products-list-item__img')
	for tab in table:
		print(tab['src'])
		url = tab['src']
		name = (', '.join(tab['alt'].split(',')[:2]) + '.jpg').replace('/', '')
		a1 = requests.get(f"https:{url}", stream=True)
		with open(name, 'wb') as f:
			for chunk in a1:
				f.write(chunk)
	return len(table) == 0


def main():
	i = 573
	while True:
		print(i)
		res = parse(get_html(f'https://www.lamoda.ru/c/477/clothes-muzhskaya-odezhda/?ajax=1..{i}'))
		if res:
			break
			i += 1


if __name__ == '__main__':
	main()
